// Logo
function Logo(selector) {
    this.$logo = $(selector);
    this.info = { "ctr":0, "panel": "" };
    this.collection = ["show-back",  "show-right", "show-left", "show-top", "show-bottom", "show-back"];
}

Logo.prototype.animate = function(){
    var _this = this;
    setTimeout(function(){
        _this.info.ctr++;
        _this.info.ctr = _this._rotate();
        _this.animate();
    }, 4000);
};

Logo.prototype._rotate = function(){
    this._toggle(this.collection[this.info.ctr-1]);
    this.info.ctr===6 ? this.info.ctr=0 : null;
    return this.info.ctr;
};


Logo.prototype._toggle = function (toShow){
    this.$logo.toggleClass(toShow);
    this.$logo.removeClass(this.info.panel);
    this.info.panel = toShow;
}

$(function(){
    var logo = new Logo('.logo');
    logo.animate();
});

// Nav
function openNav() {
    var nav = document.getElementById("nav");
    var btn = document.getElementById("navBtn");
    nav.classList.toggle("open");
    btn.classList.toggle("fa-bars");
    btn.classList.toggle("fa-times");
}